from django.test import TestCase
from django.test import Client
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
# from django.urls import resolve
# from .forms import Registration_Form
# from .views import addform
from .models import Entry


# Create your tests here.
class RegisPageUnitTest(TestCase):
    def test_regispage_url_is_exist(self):
        response = Client().get('/registrasi/')
        self.assertEqual(response.status_code, 200)

    def test_self_func_form(self):
        test = 'hehe'
        Entry.objects.create(nama=test, institusi=test, email='jaja@gmail.com', telp="085711688686")
        entry = Entry.objects.get(nama="hehe")
        self.assertEqual(str(entry), entry.nama)

    def test_logout(self):
        response = Client().get('/registrasi/logoutview')
        self.assertEqual(response.status_code, 302)

    # def test_regispage_use_addform_function(self):
    #     found = resolve('/registrasi/')
    #     self.assertEqual(found.func, addform)

    # def test_form_validation_for_telp_number_less_than(self):
    #     form = Registration_Form(data={'nama': 'George', 'institusi': 'UI', 'email': 'admin@test.com', 'telp': '123'})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors['telp'][0], "Phone number must be at least 10 characters")
    #
    # def test_form_validation_for_telp_number_is_alphabets(self):
    #     form = Registration_Form(
    #         data={'nama': 'George', 'institusi': 'UI', 'email': 'admin@test.com', 'telp': 'aaaaaaaaaaaaaa'})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors['telp'][0], "Phone number must contains only numbers")
    #
    # def test_post_success_and_render_the_result(self):
    #     test = 'hehe'
    #     response_post = Client().post('/registrasi/', {'nama': test, 'institusi': test, 'email': 'jaja@gmail.com',
    #                                                    'telp': '085711688686'})
    #     self.assertEqual(response_post.status_code, 302)
    #
