# from django import forms
# from .models import Entry
#
#
# class Registration_Form(forms.ModelForm):
#     class Meta:
#         model = Entry
#         labels = {
#             "nama": "Nama Lengkap",
#             "institusi": "Institusi/Lembaga",
#             "email": "Email",
#             "telp": "No. Telp",
#         }
#         fields = '__all__'
#         fields_required = '__all__'
#
#     def clean(self):
#         cleaned_data = super(Registration_Form, self).clean()
#         telp = cleaned_data.get('telp')
#
#         if len(telp) < 10:
#             self.add_error('telp', 'Phone number must be at least 10 characters')
#         elif not self.is_number(telp):
#             self.add_error('telp', 'Phone number must contains only numbers')
#         return cleaned_data
#
#     @staticmethod
#     def is_number(s):
#         try:
#             float(s)
#             return True
#         except ValueError:
#             return False
