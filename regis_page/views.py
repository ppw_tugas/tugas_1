from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def register(request):
    return render(request, 'Registration.html')


def logoutview(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/')

# def addform(request):
#     form = Registration_Form(request.POST or None)
#     response = {}
#     if request.method == "POST" and form.is_valid():
#         nama = request.POST.get('nama')
#         institusi = request.POST.get('institusi')
#         email = request.POST.get('email')
#         telp = request.POST.get('telp')
#         Entry.objects.create(nama=nama, institusi=institusi,
#                              email=email, telp=telp)
#         return HttpResponseRedirect('/')

#     else:
#         entry = Entry.objects.all()
#         response['form'] = form
#         response['entry'] = entry
#         return render(request, 'Registration.html', response)
