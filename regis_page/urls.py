from django.urls import path
from . import views

app_name = 'regis_page'

urlpatterns = [
    path('', views.register, name='form'),
    path('logoutview', views.logoutview, name='logoutview'),
]
