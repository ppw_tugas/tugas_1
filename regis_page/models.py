from django.db import models


# Create your models here.
class Entry(models.Model):
    nama = models.CharField(max_length=100)
    institusi = models.CharField(max_length=100)
    email = models.EmailField(unique=True, error_messages={
        'unique': "This email address is already registered, please use another email"})
    telp = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
