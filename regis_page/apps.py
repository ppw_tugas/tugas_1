from django.apps import AppConfig


class RegisPageConfig(AppConfig):
    name = 'regis_page'
