"""ppw_tugas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views
import home_page

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home_page.urls')),
    path('registrasi/', include('regis_page.urls')),
    path('donasi/', include('form_page.urls')),
    path('disasterInfo/', include('donate_page.urls')),
    path('login/', views.LoginView.as_view(), name='login'),
    path('auth/', include('social_django.urls', namespace='social')),
    path('donasi-saya/', include('listdonasi_page.urls')),
]

handler404 = home_page.views.handler404
handler500 = home_page.views.handler500
