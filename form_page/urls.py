from django.urls import path
from .views import *

urlpatterns = [
    path('<str:titleURL>/', form_page, name='donasi'),
    path('thankyou', thankyou_page, name='thankyou'),
]
