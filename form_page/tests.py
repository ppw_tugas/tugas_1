from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.apps import apps
from .apps import FormPageConfig
from donate_page.models import *
from .models import *
from regis_page.forms import *
from .forms import Donasi_Form
from urllib.parse import urlencode
from importlib import import_module
from django.conf import settings


class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class UnitTest(SessionTestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/donasi/')
        self.assertNotEqual(response.status_code, 200)

    def test_thankyou_page_is_completed(self):
        response = Client().get('/donasi/thankyou')
        html_response = response.content.decode('utf8')
        self.assertIn('Terima Kasih!', html_response)

    def test_apps(self):
        self.assertEqual(FormPageConfig.name, 'form_page')
        self.assertEqual(apps.get_app_config('form_page').name, 'form_page')

    def test_thankyou_page_is_exist(self):
        response = Client().get('/donasi/thankyou')
        self.assertEqual(response.status_code, 200)

    def test_thankyou_page_using_index_func(self):
        found = resolve('/donasi/thankyou')
        self.assertEqual(found.func, thankyou_page)

    def test_model_can_create(self):
        publisherTest = Publisher.objects.create(nama="aryo")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                          titleURL='ini-banjir',
                                          person=publisherTest)
        model_create = Donasi.objects.create(nama_donatur='nadia', email_donatur='nadia@gmail.com',
                                             jumlah_donasi='100000', anonymous=True, program=bencana)
        counting = Donasi.objects.all().count()
        self.assertEqual(counting, 1)

    def test_self_func_news(self):
        publisherTest = Publisher.objects.create(nama="aryo")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                          titleURL='ini-banjir',
                                          person=publisherTest)
        model_create = Donasi.objects.create(nama_donatur='nadia', email_donatur='nadia@gmail.com',
                                             jumlah_donasi='100000', anonymous=True, program=bencana)
        title = Donasi.objects.get(nama_donatur='nadia')
        self.assertEqual(str(title), title.nama_donatur)

    def test_form_is_valid(self):
        form = Donasi_Form(data={'nama_donatur': 'nadia', 'email_donatur': '', 'jumlah_donasi': '100000'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['email_donatur'][0], "This field is required.")

    # def test_email_is_valid(self):
    #     form_regis = form = Registration_Form(data={'nama': 'George', 'institusi': 'UI', 'email': 'admin@test.com', 'telp': '123'})
    #     form = Donasi_Form(data ={'nama_donatur':'nadia','email_donatur':'nadia@lalala.com','jumlah_donasi':'100000'})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors['email_donatur'][0], "Please register your email first.")

    def test_jumlah_donasi_shouldnot_minus(self):
        form = Donasi_Form(
            data={'nama_donatur': 'nadia', 'email_donatur': 'nadia@lalala.com', 'jumlah_donasi': '-100000'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['jumlah_donasi'][0], "Please input the right value for your donation.")

    def test_self_func_form(self):
        test = 'hehe'
        publisherTest = Publisher.objects.create(nama="aryo")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                          titleURL='ini-banjir', person=publisherTest)
        Donasi.objects.create(nama_donatur=test, email_donatur='nadia@gmail.com', jumlah_donasi='100000',
                              anonymous=True, program=bencana)
        entry = Donasi.objects.get(nama_donatur="hehe")
        self.assertEqual(str(entry), entry.nama_donatur)

    def test_post_success_and_render_the_result(self):
        publisherTest = Publisher.objects.create(nama="aryo")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                          titleURL='ini-banjir', person=publisherTest)
        test = 'hehe'
        response_post = Client().post('/donasi/ini-banjir', {'nama_donatur': test, 'email_donatur': 'nadia@lalala.com',
                                                             'jumlah_donasi': '100000', 'anonymous': True})
        self.assertEqual(response_post.status_code, 301)

    def test_donate_success(self):
        test = self.client.session
        test['first_name'] = 'jorj'
        test['last_name'] = 'matthew'
        test['email'] = 'test@gmail.com'
        test.save()

        publisherTest = Publisher.objects.create(nama="aryo")
        new_campaign = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                               titleURL='ini-banjir', person=publisherTest)
        response = self.client.post('/donasi/ini-banjir/', {'nama_donatur': 'hehe', 'email_donatur': 'nadia@lalala.com',
                                                            'jumlah_donasi': "100000", 'anonymous': True})
        self.assertEqual(response.status_code, 302)

    def test_donate_success_anonymous(self):
        test = self.client.session
        test['first_name'] = 'jorj'
        test['last_name'] = 'matthew'
        test['email'] = 'test@gmail.com'
        test.save()

        publisherTest = Publisher.objects.create(nama="aryo")
        new_campaign = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                               titleURL='ini-banjir', person=publisherTest)
        response = self.client.post('/donasi/ini-banjir/', {'nama_donatur': 'hehe', 'email_donatur': 'nadia@lalala.com',
                                                            'jumlah_donasi': "100000"})
        self.assertEqual(response.status_code, 302)

    def test_get_donate_form(self):
        publisherTest = Publisher.objects.create(nama="aryo")
        new_campaign = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000",
                                               titleURL='ini-banjir', person=publisherTest)
        response = Client().get('/donasi/ini-banjir/')
        self.assertEqual(response.status_code, 200)

    # def test_with_valid_form(self):
    #     publisherTest = Publisher.objects.create(nama="aryo")
    #     new_campaign = Disaster.objects.create(title='banjir', description='ini banjir', moneyTarget="200000000", titleURL='ini-banjir',person=publisherTest)
    #     form = Donasi_Form({"nama_donatur": "aryo", "email_donatur": "admin@ppw.com", "jumlah_donasi": "100000", "anonymous":True})
    #     response = Client().post('/donasi/ini-banjir/', form)
    #     self.assertEqual(response.status_code, 200)
