from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


# Create your models here.

class Donasi(models.Model):
    nama_donatur = models.CharField(max_length=30)
    email_donatur = models.CharField(max_length=30)
    jumlah_donasi = models.BigIntegerField(validators=[MinValueValidator(0), MaxValueValidator(999999999999999)])
    anonymous = models.BooleanField(default=False)
    program = models.ForeignKey('donate_page.Disaster', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_donatur
