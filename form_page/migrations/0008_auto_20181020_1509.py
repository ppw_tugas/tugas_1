# Generated by Django 2.1.1 on 2018-10-20 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form_page', '0007_auto_20181020_1507'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donasi',
            name='jumlah_donasi',
            field=models.BigIntegerField(),
        ),
    ]
