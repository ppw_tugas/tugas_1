from django.http import HttpResponseRedirect
from django.shortcuts import render
from donate_page.models import *
from .forms import *


# Create your views here.

def form_page(request, titleURL):
    response = {}

    if request.method == 'POST':
        if "anonymous" in list(request.POST.keys()):
            response["nama_donatur"] = request.POST["nama_donatur"]
            response["email_donatur"] = request.session['email']
            response["jumlah_donasi"] = request.POST["jumlah_donasi"]
            response["anonymous"] = request.POST["anonymous"]
            create_form = Donasi_Form(response)
        else:
            response["nama_donatur"] = request.POST["nama_donatur"]
            response["email_donatur"] = request.session['email']
            response["jumlah_donasi"] = request.POST["jumlah_donasi"]
            create_form = Donasi_Form(response)

        if create_form.is_valid():
            response['program'] = Disaster.objects.get(titleURL=titleURL)
            Donasi.objects.create(nama_donatur=response['nama_donatur'], email_donatur=response['email_donatur'],
                                  jumlah_donasi=response['jumlah_donasi'], program=response['program'])
            return HttpResponseRedirect('/donasi/thankyou')
    response = {}
    obj = Disaster.objects.get(titleURL=titleURL)
    response['form'] = Donasi_Form
    response['obj'] = obj
    return render(request, 'donasi.html', response)


def thankyou_page(request):
    return render(request, 'thankyou_page.html')
