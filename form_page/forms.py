from django import forms
from regis_page.models import Entry
from .models import Donasi


class Donasi_Form(forms.ModelForm):
    class Meta:
        model = Donasi
        fields = ['nama_donatur', 'email_donatur', 'jumlah_donasi', 'anonymous']

    attrs = {
        'class': 'form-control'
    }
    nama_donatur = forms.CharField(label='Nama Donatur', max_length=30, widget=forms.TextInput(attrs=attrs))
    email_donatur = forms.CharField(label='Email', max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'email'}))
    jumlah_donasi = forms.CharField(label='Jumlah Donasi', max_length=15, widget=forms.TextInput(attrs=attrs))
    anonymous = forms.BooleanField(label='Donasi sebagai anonim', initial=False, required=False)

    def clean(self):
        cleaned_data = super(Donasi_Form, self).clean()
        email_donatur = cleaned_data.get('email_donatur')
        # email_in_db = Entry.objects.filter(email=email_donatur)
        donasi = cleaned_data.get('jumlah_donasi')

        # if not email_in_db.exists():
        #     self.add_error('email_donatur', 'Please register your email first.')
        if int(donasi) <= 0:
            self.add_error('jumlah_donasi', 'Please input the right value for your donation.')
