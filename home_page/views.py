from django.http import JsonResponse
from django.shortcuts import render
from django.views.defaults import page_not_found
from .models import *
from donate_page.models import Disaster

# Create your views here.
content = {}


def home(request):
    if request.user.is_authenticated:
        request.session['first_name'] = request.user.first_name
        request.session['last_name'] = request.user.last_name
        request.session['email'] = request.user.email
    news = list(News.objects.all())
    news.reverse()
    content['news'] = news
    disaster = list(Disaster.objects.all())
    disaster.reverse()
    content['disasters'] = disaster
    return render(request, 'home/homepage.html', content)


def about(request):
    data = list(Testimony.objects.all())
    data.reverse()
    content['data'] = data
    return render(request, 'about/about-us.html', content)


def save_testimony(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.session['first_name'] + " " + request.session['last_name']
        Testimony.objects.create(name=name, text=text)
        return JsonResponse({'saved': True, 'name': name, 'text': text})
    return JsonResponse({'saved': False})


def handler404(request, exception):
    return page_not_found(request, exception, 'errors/404.html')


def handler500(request):
    return render(request, 'errors/500.html', status=500)
