from django.db import models


# Create your models here.
class Campaign(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=300)
    campaigner = models.CharField(max_length=60)
    image_url = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class News(models.Model):
    news = models.CharField(max_length=100)
    news_source = models.CharField(max_length=150)
    image_url = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.news


class Testimony(models.Model):
    name = models.CharField(max_length=60)
    text = models.TextField(max_length=300)

    def __str__(self):
        return self.name
