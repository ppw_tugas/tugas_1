from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *
from .models import *


# Create your tests here.

class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class HomePageUnitTest(SessionTestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_login_home_page(self):
        test = self.client.session
        test['first_name'] = 'jorj'
        test['last_name'] = 'matthew'
        test['email'] = 'test@gmail.com'
        test.save()

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_home_function(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_page_contains_brand(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('SinarPerak', html_response)

    def test_campaign_model_can_add_new(self):
        Campaign.objects.create(image_url="", title="test", description="test", campaigner="me")
        campaign_counts_all_entries = Campaign.objects.all().count()
        self.assertEqual(campaign_counts_all_entries, 1)

    def test_news_model_can_add_new(self):
        News.objects.create(news='Test', image_url='')
        news_counts_all_entries = News.objects.all().count()
        self.assertEqual(news_counts_all_entries, 1)

    def test_profile_page_using_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/homepage.html')

    def test_self_func_news(self):
        News.objects.create(news='Test', image_url='')
        news = News.objects.get(news='Test')
        self.assertEqual(str(news), news.news)

    def test_self_func_campaign(self):
        Campaign.objects.create(title="test", description="test", image_url="", campaigner="me")
        campaign = Campaign.objects.get(title="test")
        self.assertEqual(str(campaign), campaign.title)

    def test_404_page(self):
        response = Client().get('/aa')
        self.assertEqual(response.status_code, 404)

    # def test_500_page(self):
    #     test = self.client.session
    #     test['first_name'] = 'jorj'
    #     test['last_name'] = 'matthew'
    #     test['email'] = 'test@gmail.com'
    #     test.save()
    #
    #     response = handler500(request)
    #     self.assertEqual(response.status_code, 200)


class AboutUsPageUnitTest(SessionTestCase):
    def test_about_page_url_is_exists(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_about_function(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_about_page_using_templates(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about/about-us.html')

    def test_testimony_model_can_add_new(self):
        Testimony.objects.create(name='Test', text='test123')
        testimony_counts_all_entries = Testimony.objects.all().count()
        self.assertEqual(testimony_counts_all_entries, 1)

    def test_self_func_name(self):
        Testimony.objects.create(name='Test', text='test123')
        testi = Testimony.objects.get(name='Test')
        self.assertEqual(str(testi), testi.name)

    def test_ajax(self):
        test = self.client.session
        test['first_name'] = 'jorj'
        test['last_name'] = 'matthew'
        test['email'] = 'test@gmail.com'
        test.save()

        resp = self.client.post('/save/', data={'text': 'mantap'})
        self.assertEqual(resp.status_code, 200)

    def test_ajax_fail(self):
        test = self.client.session
        test['first_name'] = 'jorj'
        test['last_name'] = 'matthew'
        test['email'] = 'test@gmail.com'
        test.save()

        response = self.client.get('/save/').json()
        bool = response['saved']
        self.assertFalse(bool)
