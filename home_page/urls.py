from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name="homepage"),
    path('about/', about, name="about"),
    path('save/', save_testimony, name="save-testi")
]
