# Tugas 1 PPW - CROWD FUNDING

```
Anggota kelompok:
1. Azzahra Putri - 1706025125
2. George Matthew L - 1706043891
3. Irfan Amrullah - 1706039585
4. Nadia Syafitri - 1706022672
```

Status
---
![coverage](https://gitlab.com/ppw_tugas/tugas_1/badges/master/coverage.svg)
![build](https://gitlab.com/ppw_tugas/tugas_1/badges/master/build.svg)

Heroku
---
http://crowdfunding-ppw.herokuapp.com