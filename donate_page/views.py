from django.shortcuts import render
from .models import Disaster
from form_page.models import Donasi

response = {}


def disasterInfo(request, titleURL):
    obj = Disaster.objects.get(titleURL=titleURL)
    donations = Donasi.objects.filter(program=obj)
    total = 0
    for donation in donations:
        total += int(donation.jumlah_donasi)
    total = str(total)
    response['obj'] = obj
    response['total_donation'] = total
    return render(request, 'disaster_info.html', response)

def allDonation(request):
    obj =  Disaster.objects.all()
    response['obj'] = obj
    return render(request, 'allDonation.html', response)
