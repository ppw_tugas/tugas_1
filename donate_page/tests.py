from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from .views import disasterInfo
from .models import Disaster, Publisher
from form_page.models import Donasi


class DonatePage(TestCase):

    def test_lab6_url_does_exist(self):
        publisherTest = Publisher.objects.create(nama="irfan")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', titleURL='ini-banjir',
                                          person=publisherTest)
        response = Client().get('/disasterInfo/' + bencana.titleURL + "/")
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_disaster(self):
        publisherTest = Publisher.objects.create(nama="aryo")
        bencana = Disaster.objects.create(title='banjir', description='ini banjir', titleURL='ini-aryo',
                                          person=publisherTest)

        counting_all_available_activity = Disaster.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    def test_model_can_create_new_publisher(self):
        publisherNew = Publisher.objects.create(nama="ray")
        publisher = Publisher.objects.all().count()
        self.assertEqual(publisher, 1)

    def test_self_func_title_disaster(self):
        publisherTest = Publisher.objects.create(nama="test")
        Disaster.objects.create(title='Test', description='ini test', titleURL='ini-test', person=publisherTest)
        title = Disaster.objects.get(title='Test')
        self.assertEqual(str(title), title.title)

    def test_does_importing_donasi_model_working(self):
        request = HttpRequest()
        publisherTest = Publisher.objects.create(nama="test")
        disasterTest = Disaster.objects.create(title='Test', description='ini test', titleURL='ini-test-2',
                                               person=publisherTest)
        newDonatur = Donasi.objects.create(nama_donatur="irfan", email_donatur="irfan@gmail.com",
                                           jumlah_donasi=int("100000"), program=disasterTest)
        donations = Donasi.objects.filter(program=Disaster.objects.get(titleURL='ini-test-2'))
        response = disasterInfo(request, 'ini-test-2')
        html_response = response.content.decode('utf8')
        self.assertIn('Rp 100,000', html_response)

class Donation(TestCase):
    
    def test_all_programs_url_does_exist(self):
        response = Client().get('/disasterInfo/allDonation')
        self.assertEqual(response.status_code, 200)
