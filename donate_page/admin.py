from django.contrib import admin
from .models import Disaster, Publisher

# Register your models here.
admin.site.register(Disaster)
admin.site.register(Publisher)
