from django.urls import path
from .views import *

app_name = 'donate_page'
urlpatterns = [
    path('<str:titleURL>/', disasterInfo, name='disasterInfo'),
    path('allDonation', allDonation, name='allDonation')
]
