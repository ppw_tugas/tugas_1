from django.apps import AppConfig


class DonatePageConfig(AppConfig):
    name = 'donate_page'
