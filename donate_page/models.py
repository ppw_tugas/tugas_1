from django.db import models


# Create your models here.
class Disaster(models.Model):
    title = models.CharField(max_length=50)
    imageURL = models.CharField(max_length=300, null=True)
    description = models.TextField()
    moneyTarget = models.CharField(max_length=30, null=True)
    titleURL = models.CharField(max_length=300, primary_key=True)
    person = models.ForeignKey('Publisher', on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Publisher(models.Model):
    nama = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.nama
