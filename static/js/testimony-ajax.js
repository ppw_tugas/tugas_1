function saveTestimony() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var text = $('#comment').val();
    $.ajax({
        method: "POST",
        url: "/save/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            text: text,
        },
        success: function (result) {
            if (result.saved) {
                var html = `<div class="testimony-set d-flex justify-content-center align-items-center flex-column"><p class="testimony-txt">“${result.text}”</p><p class="testimoners"><strong>${result.name}</strong></p></div>`;
                $('#testimony').prepend(html);
                $('#comment').val('');
            }
        },
        error: function () {
            alert("Error, cannot save data to server");
        }
    })
}