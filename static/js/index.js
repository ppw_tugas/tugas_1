$(document).ready(function () {

    // Smooth scrolling effect
    $("#donate-button").click(function () {
        $('html, body').animate({
            scrollTop: $("#donation").offset().top
        }, 800);
    });

    $("#link-to-news").click(function () {
        $('html, body').animate({
            scrollTop: $("#news").offset().top
        }, 800);
    });

    // Dynamic navbar
    var donation = $('#donation').offset().top;
    $(document).scroll(function () {
        var scrollPos = $(document).scrollTop();
        if ($(window).width() > 767) {
            if (scrollPos >= donation) {
                $('#nav').css('background-color', '#541D38');
            } else if (scrollPos <= donation) {
                $('#nav').css('background-color', 'transparent');
            }
        }
    });

});
