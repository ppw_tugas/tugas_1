from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core import serializers
from form_page.models import *


# Create your views here.
def index(request):
    return render(request, 'list_donasi.html')


def list_donasi(request):
    if request.user.is_authenticated:
        email = request.user.email
        donation_data = Donasi.objects.filter(email_donatur=email)
        return HttpResponse(serializers.serialize('json', donation_data), content_type='application/json')
    return HttpResponseRedirect("registrasi/")
