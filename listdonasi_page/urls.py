from django.urls import path
from listdonasi_page.views import *

urlpatterns = [
    path('', index , name='list_donasi'),
    path('listdonasi', list_donasi, name='data_donasi'),
]
