from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from listdonasi_page.apps import ListdonasiPageConfig
from listdonasi_page.views import index as list_func


# Create your tests here.

class ListDonasiPageUnitTest(TestCase):
    def test_list_donasi_url_is_exist(self):
        response = Client().get('/donasi-saya/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_is_exist(self):
        response = Client().get('/donasi-saya/listdonasi')
        self.assertEqual(response.status_code, 302)

    def test_list_donasi_page_using_index_func(self):
        found = resolve('/donasi-saya/')
        self.assertEqual(found.func, list_func)

    def test_list_donasi_page_using_templates(self):
        response = Client().get('/donasi-saya/')
        self.assertTemplateUsed(response, 'list_donasi.html')

    def test_apps(self):
        self.assertEqual(ListdonasiPageConfig.name, 'listdonasi_page')
        self.assertEqual(apps.get_app_config('listdonasi_page').name, 'listdonasi_page')
